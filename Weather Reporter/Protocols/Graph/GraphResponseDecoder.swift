//
//  GraphResponseDecoder.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

protocol GraphDecoder {
    init(json: [String: Any])
    func toJson() -> [String: Any]?
}
