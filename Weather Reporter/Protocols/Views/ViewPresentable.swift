//
//  ViewPresentable.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import UIKit

protocol ViewPresentable {
    var viewController: UIViewController? { get set }
    var navigationController: UINavigationController? { get set }
    var view: UIView? { get set }
}
