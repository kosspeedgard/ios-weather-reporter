//
//  TableViewPresentable.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import UIKit

@objc protocol TableViewPresentable {
    func numberOfRows(section: Int) -> Int
    func cellForRow(with tableView: UITableView, and indexPath: IndexPath) -> UITableViewCell
    
    @objc optional func numberOfSections() -> Int
    @objc optional func willDisplay(cell: UITableViewCell)
}
