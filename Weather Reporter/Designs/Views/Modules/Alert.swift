//
//  Alert.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import UIKit

class AlertController: UIViewController {
    var alertTitle: String?
    var message: String?
    var haveMinorButton: Bool?
    
    var majorTitle: String?
    var majorButtonAction: ((UIAlertAction) -> Void)?
    
    var minorTitle: String?
    var minorButtonAction: ((UIAlertAction) -> Void)?
    
    var viewController: UIViewController?
    
    func show() {
        let alertController = UIAlertController(title: alertTitle ?? "", message: message, preferredStyle: .alert)
        let majorAction = UIAlertAction(title: majorTitle ?? "", style: .default, handler: majorButtonAction)
        let minorAction = UIAlertAction(title: minorTitle ?? "", style: .cancel, handler: minorButtonAction)
        
        alertController.addAction(majorAction)
        
        if haveMinorButton! {
            alertController.addAction(minorAction)
        }
        
        doOnMain {
            self.viewController?.present(alertController, animated: true, completion: nil)
        }
    }
}
