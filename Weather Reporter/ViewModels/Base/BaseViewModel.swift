//
//  BaseViewModel.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import UIKit

class BaseViewModel: NSObject, ViewPresentable {
    var viewController: UIViewController?
    var navigationController: UINavigationController?
    var view: UIView?
}
