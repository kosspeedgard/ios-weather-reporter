//
//  WeatherViewModel.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import UIKit

class WeatherViewModel: BaseViewModel {
    private weak var delegate: WeatherViewModelDelegate?
    
    private var weatherModel: WeatherModel?
    
    private var weatherBase: GraphWeatherBase?
    private var forecastBase: GraphForecastBase?
    
    var receivedCity: ((_ city: String) -> Void)?
    var receivedTemperature: ((_ temperature: String) -> Void)?
    var receivedHumidity: ((_ himidity: String) -> Void)?
    var receivedWind: ((_ wind: String) -> Void)?
    var receivedText: ((_ text: String?) -> Void)?
    var receivedConvertType: ((_ text: String) -> Void)?
    var hiddenSearchButton: ((_ width: CGFloat) -> Void)?
    
    var city: String?
    
    var isCelsius: Bool = true
    var searchButtonWidth: CGFloat = 0
    
    init(delegate: WeatherViewModelDelegate) {
        super.init()
        self.delegate = delegate
        weatherModel = WeatherModel()
        
//        Timer.scheduledTimer(withTimeInterval: 60, repeats: true, block: { [unowned self] (timer) -> Void in
//            self.fetchWeather()
//        })
    }
}

//MARK: Manage Data
extension WeatherViewModel {
    func fetchWeather() {
        weatherModel?.fetchWeather(city: city, completion: { [weak self] (weatherBase) -> Void in
            self?.weatherBase = weatherBase
            self?.binding()
        }, failure: { (error) -> Void in
            self.alert(error: error)
        })
    }
    
    func fetchForecast() {
        weatherModel?.fetchForecast(city: city, completion: { [weak self] (forecastBase) -> Void in
            self?.forecastBase = forecastBase
            self?.delegate?.didFetchForecastData()
        }, failure: { (error) -> Void in
            self.alert(error: error)
        })
    }
    
    func binding() {
        let temperature = isCelsius ? weatherModel?.covertToCelsius(kelvin: weatherBase?.main?.temp ?? 0.0) : weatherModel?.convertToFarenheit(kelvin: weatherBase?.main?.temp ?? 0.0)
        
        receivedTemperature?("\(temperature ?? 0)°")
        receivedHumidity?("\(weatherBase?.main?.humidity ?? 0)%")
        receivedWind?("\(weatherBase?.wind?.speed ?? 0) km/hr")
        receivedCity?("\(weatherBase?.name ?? "")")
        receivedConvertType?("\(isCelsius ? Constants.Wording.toF : Constants.Wording.toC)")
    }
    
    func convertTemperatureType() {
        isCelsius = !isCelsius
        
        binding()
        delegate?.didFetchForecastData()
    }
}

//MARK: Manage View
extension WeatherViewModel {
    func visibilitySearchButton() {
        if let text = city, !text.isEmpty {
            if searchButtonWidth == 60 {
                hiddenSearchButton?(0)
            }
            else {
                hiddenSearchButton?(60)
            }
        }
        else {
            hiddenSearchButton?(0)
        }
    }
    
    private func alert(error: GraphError?) {
        let alertController = AlertController()
        alertController.alertTitle = Constants.Alert.error
        alertController.message = error?.message
        alertController.haveMinorButton = false
        alertController.majorTitle = Constants.Alert.ok
        alertController.viewController = viewController
        alertController.majorButtonAction = { [unowned self] (action) -> Void in
            self.city = nil
            self.receivedText?(self.city)
        }
        
        alertController.show()
    }
}

//MARK: TableViewPresentable
extension WeatherViewModel: TableViewPresentable {
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows(section: Int) -> Int {
        return forecastBase?.forecasts?.count ?? 0
    }
    
    func willDisplay(cell: UITableViewCell) {
        cell.backgroundColor = .clear
    }
    
    func cellForRow(with tableView: UITableView, and indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.Weather.forecastTableViewCell, for: indexPath) as! ForecastTableViewCell
        let forcast = forecastBase?.forecasts?[indexPath.row]
        let temperature = isCelsius ? weatherModel?.covertToCelsius(kelvin: forcast?.main?.temperature ?? 0.0) : weatherModel?.convertToFarenheit(kelvin: forcast?.main?.temperature ?? 0.0)
        
        cell.dayLabel.text = Utils.timeFromUnix(time: forcast?.dateTime ?? 0, format: Constants.TimeFormat.shortTime)
        cell.descriptionLabel.text = forcast?.weather?.first?.description ?? ""
        cell.temperatureLabel.text = "\(temperature ?? 0)°"
        cell.humidityLabel.text = "\(forcast?.main?.humidity ?? 0)%"
        cell.windLabel.text = "\(forcast?.wind?.speed ?? 0) km/hr"
        
        return cell
    }
}
