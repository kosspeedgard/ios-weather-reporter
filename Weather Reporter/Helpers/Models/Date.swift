//
//  Date.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

extension Date {
    init(unix:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(unix))
    }
    
    func string(format: String) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
