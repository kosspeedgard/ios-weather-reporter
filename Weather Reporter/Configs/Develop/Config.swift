//
//  Config.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct Config {
    static let apiKey = "&APPID=f4f7e9f29295684d802b275ad6ca6d5c"
    
    //MARK: Server
    enum Server: String {
        case baseURL = "https://api.openweathermap.org/"
    }
}
