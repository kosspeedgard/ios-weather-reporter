//
//  URLSuffixs.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct URLSuffixs {
    //MARK: Weather
    struct Weather {
        static let weather = "data/2.5/weather?"
        static let forecast = "data/2.5/forecast?"
    }
}
