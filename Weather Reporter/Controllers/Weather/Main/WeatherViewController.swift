//
//  WeatherViewController.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var convertTypeButton: UIButton!
    
    //Constraint
    @IBOutlet weak var searchButtonWidthConstraint: NSLayoutConstraint!
    
    private var viewModel: WeatherViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        binding()
        setupViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func setup() {
        viewModel = WeatherViewModel(delegate: self)
        viewModel?.viewController = self
        viewModel?.navigationController = navigationController
        viewModel?.view = view
        
        viewModel?.fetchWeather()
        viewModel?.fetchForecast()
    }
    
    fileprivate func setupViews() {
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        tableView.backgroundColor = .clear
        tableView.layer.borderColor = UIColor.white.cgColor
        tableView.layer.borderWidth = 0.5
        tableView.layer.cornerRadius = 8
        
        convertTypeButton.layer.borderColor = UIColor.black.cgColor
        convertTypeButton.layer.borderWidth = 1
        convertTypeButton.layer.cornerRadius = convertTypeButton.bounds.size.height / 2
        
        searchTextField.keyboardType = .asciiCapable
        
        viewModel?.searchButtonWidth = searchButtonWidthConstraint.constant
    }
    
    fileprivate func binding() {
        viewModel?.receivedCity = { [unowned self] (city) -> Void in
            self.cityLabel.text = city
        }
        
        viewModel?.receivedTemperature = { [unowned self] (temperature) -> Void in
            self.temperatureLabel.text = temperature
        }
        
        viewModel?.receivedHumidity = { [unowned self] (humidity) -> Void in
            self.humidityLabel.text = humidity
        }
        
        viewModel?.receivedWind = { [unowned self] (wind) -> Void in
            self.windLabel.text = wind
        }
        
        viewModel?.receivedText = { [unowned self] (text) -> Void in
            self.searchTextField.text = text
        }
        
        viewModel?.receivedConvertType = { [unowned self] (type) -> Void in
            self.convertTypeButton.setTitle(type, for: .normal)
        }
        
        viewModel?.hiddenSearchButton = { [unowned self] (width) -> Void in
            UIView.animate(withDuration: 0.1, animations: {
                self.searchButtonWidthConstraint.constant = width
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func onTappedSearch(_ sender: Any) {
        viewModel?.fetchWeather()
        viewModel?.fetchForecast()
    }
    
    @IBAction func onTappedConvertType(_ sender: Any) {
        viewModel?.convertTemperatureType()
    }
    
    @IBAction func onTappedAtScreen(_ sender: Any) {
        view.endEditing(true)
    }
}

//MARK: UITableViewDelegate, UITableViewDataSource
extension WeatherViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.numberOfSections() ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows(section: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewModel?.willDisplay(cell: cell)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = viewModel?.cellForRow(with: tableView, and: indexPath)
        return cell!
    }
}

//MARK: WeatherViewModelDelgate
extension WeatherViewController: WeatherViewModelDelegate {
    func didFetchForecastData() {
        tableView.reloadData()
    }
}

//MARK: UITextFieldDelegate
extension WeatherViewController: UITextFieldDelegate {
    @objc fileprivate func textFieldDidChange(_ textField: UITextField) {
        viewModel?.city = textField.text
        viewModel?.visibilitySearchButton()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewModel?.city = textField.text
        viewModel?.visibilitySearchButton()
    }
}
