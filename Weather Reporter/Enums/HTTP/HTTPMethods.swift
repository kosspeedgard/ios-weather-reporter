//
//  HTTPMethods.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

enum HTTPMethods: String {
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
    case DELETE = "DELETE"
}
