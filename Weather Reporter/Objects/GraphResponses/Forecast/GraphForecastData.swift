//
//  GraphForecastListData.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphForecastData: GraphDecoder {
    var dateTime: Int?
    var main: GraphForecastMain?
    var weather: [GraphForecastWeather]?
    var clouds: GraphForecastClouds?
    var wind: GraphForecastWind?
    var rain: GraphForecastRain?
    var system: GraphForecastSystem?
    var dateTimeText: String?
    
    init(json: [String : Any]) {
        weather = [GraphForecastWeather]()
        
        if let _dateTime = json["dt"] as? Int {
            self.dateTime = _dateTime
        }
        
        if let _main = json["main"] as? [String: Any] {
            self.main = GraphForecastMain(json: _main)
        }
        
        if let _weather = json["weather"] as? [[String: Any]] {
            for json in _weather {
                let weather = GraphForecastWeather(json: json)
                self.weather?.append(weather)
            }
        }
        
        if let _clouds = json["clouds"] as? [String: Any] {
            self.clouds = GraphForecastClouds(json: _clouds)
        }
        
        if let _wind = json["wind"] as? [String: Any] {
            self.wind = GraphForecastWind(json: _wind)
        }
        
        if let _rain = json["rain"] as? [String: Any] {
            self.rain = GraphForecastRain(json: _rain)
        }
        
        if let _system = json["sys"] as? [String: Any] {
            self.system = GraphForecastSystem(json: _system)
        }
        
        if let _dateTimeText = json["dt_txt"] as? String {
            self.dateTimeText = _dateTimeText
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
