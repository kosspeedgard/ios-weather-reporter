//
//  GraphForecastClouds.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphForecastClouds: GraphDecoder {
    var all: Int?
    
    init(json: [String : Any]) {
        if let _all = json["all"] as? Int {
            self.all = _all
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
