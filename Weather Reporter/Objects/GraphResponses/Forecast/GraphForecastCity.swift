//
//  GraphForecastCity.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphForecastCity: GraphDecoder {
    var id: Int?
    var name: String?
    var coordinate: GraphForecastCoordinate?
    var country: String?
    var population: Int?
    
    init(json: [String : Any]) {
        if let _id = json["id"] as? Int {
            self.id = _id
        }
        
        if let _name = json["name"] as? String {
            self.name = _name
        }
        
        if let _coordinate = json["coord"] as? [String: Any] {
            self.coordinate = GraphForecastCoordinate(json: _coordinate)
        }
        
        if let _country = json["country"] as? String {
            self.country = _country
        }
        
        if let _population = json["population"] as? Int {
            self.population = _population
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
