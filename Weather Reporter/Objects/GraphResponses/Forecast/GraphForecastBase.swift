//
//  GraphForecastBase.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphForecastBase: GraphDecoder {
    var code: Int?
    var message: Double?
    var count: Int?
    var forecasts: [GraphForecastData]?
    var city: GraphForecastCity?
    
    init(json: [String : Any]) {
        forecasts = [GraphForecastData]()
        
        if let _code = json["cod"] as? Int {
            self.code = _code
        }
        
        if let _message = json["message"] as? Double {
            self.message = _message
        }
        
        if let _count = json["cnt"] as? Int {
            self.count = _count
        }
        
        if let _forecasts = json["list"] as? [[String: Any]] {
            for json in _forecasts {
                let forecast = GraphForecastData(json: json)
                self.forecasts?.append(forecast)
            }
        }
        
        if let _city = json["city"] as? [String: Any] {
            self.city = GraphForecastCity(json: _city)
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
