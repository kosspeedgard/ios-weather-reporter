//
//  GraphForecastWind.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphForecastWind: GraphDecoder {
    var speed: Double?
    var deg: Double?
    
    init(json: [String : Any]) {
        if let _speed = json["speed"] as? Double {
            self.speed = _speed
        }
        
        if let _deg = json["deg"] as? Double {
            self.deg = _deg
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
