//
//  GraphForecastMain.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphForecastMain: GraphDecoder {
    var temperature: Double?
    var tempMin: Double?
    var tempMax: Double?
    var pressure: Double?
    var seaLevel: Double?
    var groundLvel: Double?
    var humidity: Double?
    var tempKF: Double?
    
    init(json: [String : Any]) {
        if let _temperature = json["temp"] as? Double {
            self.temperature = _temperature
        }
        
        if let _tempMin = json["temp_min"] as? Double {
            self.tempMin = _tempMin
        }
        
        if let _tempMax = json["temp_max"] as? Double {
            self.tempMax = _tempMax
        }
        
        if let _pressure = json["pressure"] as? Double {
            self.pressure = _pressure
        }
        
        if let _seaLevel = json["sea_level"] as? Double {
            self.seaLevel = _seaLevel
        }
        
        if let _groundLevel = json["grnd_level"] as? Double {
            self.groundLvel = _groundLevel
        }
        
        if let _humidity = json["humidity"] as? Double {
            self.humidity = _humidity
        }
        
        if let _tempKF = json["temp_kf"] as? Double {
            self.tempKF = _tempKF
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
