//
//  GraphForecastSystem.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphForecastSystem: GraphDecoder {
    var pod: String?
    
    init(json: [String : Any]) {
        if let _pod = json["pod"] as? String {
            self.pod = _pod
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
