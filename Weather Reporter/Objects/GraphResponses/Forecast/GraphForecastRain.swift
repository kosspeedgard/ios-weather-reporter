//
//  GraphForecastRain.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphForecastRain: GraphDecoder {
    var _3h: Double?
    
    init(json: [String : Any]) {
        if let __3h = json["3h"] as? Double {
            self._3h = __3h
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
