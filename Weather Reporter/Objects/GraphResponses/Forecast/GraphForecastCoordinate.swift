//
//  GraphForecastCoordinate.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphForecastCoordinate: GraphDecoder {
    var latitude: Double?
    var longitude: Double?
    
    init(json: [String : Any]) {
        if let _latitude = json["lat"] as? Double {
            self.latitude = _latitude
        }
        
        if let _longitude = json["lon"] as? Double {
            self.longitude = _longitude
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
