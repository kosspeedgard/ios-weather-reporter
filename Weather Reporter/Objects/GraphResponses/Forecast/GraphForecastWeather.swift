//
//  GraphForecastWeather.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphForecastWeather: GraphDecoder {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
    
    init(json: [String : Any]) {
        if let _id = json["id"] as? Int {
            self.id = _id
        }
        
        if let _main = json["main"] as? String {
            self.main = _main
        }
        
        if let _description = json["description"] as? String {
            self.description = _description
        }
        
        if let _icon = json["icon"] as? String {
            self.icon = _icon
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
