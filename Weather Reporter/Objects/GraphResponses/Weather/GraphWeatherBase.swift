//
//  GraphWeatherBase.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphWeatherBase: GraphDecoder {
    var coordinate: GraphWeatherCoordinate?
    var weather: GraphWeather?
    var base: String?
    var main: GraphWeatherMain?
    var visibility: Int?
    var wind: GraphWeatherWind?
    var clouds: GraphWeatherClouds?
    var dt: Int?
    var system: GraphWeatherSystem?
    var id: Int?
    var name: String?
    var code: Int?
    
    init(json: [String : Any]) {
        if let _coordinate = json["coord"] as? [String: Any] {
            self.coordinate = GraphWeatherCoordinate(json: _coordinate)
        }
        
        if let _weather = json["weather"] as? [String: Any] {
            self.weather = GraphWeather(json: _weather)
        }
        
        if let _base = json["base"] as? String {
            self.base = _base
        }
        
        if let _main = json["main"] as? [String: Any] {
            self.main = GraphWeatherMain(json: _main)
        }
        
        if let _visibility = json["visibility"] as? Int {
            self.visibility = _visibility
        }
        
        if let _wind = json["wind"] as? [String: Any] {
            self.wind = GraphWeatherWind(json: _wind)
        }
        
        if let _clouds = json["clouds"] as? [String: Any] {
            self.clouds = GraphWeatherClouds(json: _clouds)
        }
        
        if let _dt = json["dt"] as? Int {
            self.dt = _dt
        }
        
        if let _system = json["system"] as? [String: Any] {
            self.system = GraphWeatherSystem(json: _system)
        }
        
        if let _id = json["id"] as? Int {
            self.id = _id
        }
        
        if let _name = json["name"] as? String {
            self.name = _name
        }
        
        if let _code = json["code"] as? Int {
            self.code = _code
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
