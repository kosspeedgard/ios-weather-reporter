//
//  GraphWeatherClouds.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphWeatherClouds: GraphDecoder {
    var all: Double?
    
    init(json: [String : Any]) {
        if let _all = json["all"] as? Double {
            self.all = _all
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
