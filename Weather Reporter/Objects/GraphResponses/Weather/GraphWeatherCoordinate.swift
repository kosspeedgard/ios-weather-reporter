//
//  GraphWeatherCoordinate.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphWeatherCoordinate: GraphDecoder {
    var longitude: Double?
    var latitude: Double?
    
    init(json: [String : Any]) {
        if let _longitude = json["lon"] as? Double {
            self.longitude = _longitude
        }
        
        if let _latitude = json["lat"] as? Double {
            self.longitude = _latitude
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
