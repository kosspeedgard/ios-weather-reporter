//
//  GraphWeatherMain.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphWeatherMain: GraphDecoder {
    var temp: Double?
    var pressure: Double?
    var humidity: Double?
    var tempMin: Double?
    var tempmax: Double?
    
    init(json: [String : Any]) {
        if let _temp = json["temp"] as? Double {
            self.temp = _temp
        }
        
        if let _pressure = json["pressure"] as? Double {
            self.pressure = _pressure
        }
        
        if let _humidity = json["humidity"] as? Double {
            self.humidity = _humidity
        }
        
        if let _tempMin = json["temp_min"] as? Double {
            self.tempMin = _tempMin
        }
        
        if let _tempMax = json["temp_max"] as? Double {
            self.tempmax = _tempMax
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
