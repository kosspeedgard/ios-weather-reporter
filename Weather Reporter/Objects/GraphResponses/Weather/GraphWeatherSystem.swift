//
//  GraphWeatherSystem.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphWeatherSystem: GraphDecoder {
    var type: Int?
    var id: Int?
    var message: Double?
    var country: String?
    var sunrise: Int?
    var sunset: Int?
    
    init(json: [String : Any]) {
        if let _type = json["type"] as? Int {
            self.type = _type
        }
        
        if let _id = json["id"] as? Int {
            self.id = _id
        }
        
        if let _message = json["message"] as? Double {
            self.message = _message
        }
        
        if let _country = json["country"] as? String {
            self.country = _country
        }
        
        if let _sunrise = json["sunrise"] as? Int {
            self.sunrise = _sunrise
        }
        
        if let _sunset = json["sunset"] as? Int {
            self.sunset = _sunset
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
