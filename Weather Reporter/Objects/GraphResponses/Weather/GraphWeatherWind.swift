//
//  GraphWeatherWind.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct GraphWeatherWind: GraphDecoder {
    var speed: Double?
    var deg: Int?
    
    init(json: [String : Any]) {
        if let _speed = json["speed"] as? Double {
            self.speed = _speed
        }
        
        if let _deg = json["deg"] as? Int {
            self.deg = _deg
        }
    }
    
    func toJson() -> [String : Any]? {
        return nil
    }
}
