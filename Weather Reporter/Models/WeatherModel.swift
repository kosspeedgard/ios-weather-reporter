//
//  WeatherModel.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

class WeatherModel: BaseModel {
    func fetchWeather(city: String?, completion: @escaping(_ weatherBase: GraphWeatherBase?) -> Void, failure: @escaping(_ error: GraphError?) -> Void) {
        super.sendRequestWith(method: .GET, server: .baseURL, urlSuffixs: URLSuffixs.Weather.weather + "q=\(city ?? "bangkok")" , params: nil, responseType: GraphWeatherBase.self, completion: { (weatherBase) -> Void in
            completion(weatherBase)
        }, failure: { (error) -> Void in
            failure(error)
        })
    }
    
    func fetchForecast(city: String?, completion: @escaping(_ forecastBase: GraphForecastBase?) -> Void, failure: @escaping(_ error: GraphError?) -> Void) {
        super.sendRequestWith(method: .GET, server: .baseURL, urlSuffixs: URLSuffixs.Weather.forecast + "q=\(city ?? "bangkok")&cnt=7", params: nil, responseType: GraphForecastBase.self, completion: { (forecastBase) -> Void in
            completion(forecastBase)
        }, failure: { (error) -> Void in
            failure(error)
        })
    }
    
    func covertToCelsius(kelvin: Double) -> Int {
        let celsius = kelvin - 273.15
        return Int(celsius)
    }
    
    func convertToFarenheit(kelvin: Double) -> Int {
        let farenheit = 9 / 5 * (kelvin - 273.15) + 32
        return Int(farenheit)
    }
}
