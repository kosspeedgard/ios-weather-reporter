//
//  BaseModel.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

class BaseModel: NSObject, URLSessionDelegate {
    
    func sendRequestWith<T: GraphDecoder>(method: HTTPMethods, server: Config.Server, urlSuffixs: String, params: [String: Any]?, responseType: T.Type, completion: @escaping(_ graph: T?) -> Void, failure: @escaping(_ error: GraphError?) -> Void) {
        let url = server.rawValue + urlSuffixs + Config.apiKey
        
        requestWith(method: method, url: url, params: params, responseType: responseType, completion: { (graph) -> Void in
            completion(graph)
        }, failure: { (error) -> Void in
            failure(error)
        })
    }
    
    private func requestWith<T: GraphDecoder>(method: HTTPMethods, url: String, params: [String: Any]?, responseType: T.Type, completion: @escaping(_ graph: T?) -> Void, failure: @escaping(_ error: GraphError?) -> Void) {
        
        var urlRequest = URLRequest(url: URL(string: url)!)
        urlRequest.httpMethod = method.rawValue
        
        if let _params = params {
            urlRequest.httpBody = JSONUtils.convertDictToJsonString(dictionary: _params)?.data(using: .utf8)
        }
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 15.0
        configuration.timeoutIntervalForResource = 15.0
        
        let urlSession = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        let dataTask = urlSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                let message = "Send \(method) is error: \(String(describing: error?.localizedDescription))"
                let error = self.handleLocalError(message: message, code: ((error as NSError?)?.code) ?? 0)

                doOnMain {
                    failure(error)
                }
                
                return
            }
            
            if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                switch statusCode {
                case 200...299:
                    let graph = self.decoder(data: data!, responseType: responseType)
                    
                    doOnMain {
                        completion(graph)
                    }
                    
                    break
                default:
                    let error = self.decoder(data: data!, responseType: GraphError.self)

                    doOnMain {
                        failure(error)
                    }
                    
                    break
                }
            }
        })
        
        dataTask.resume()
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let urlCredential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
        completionHandler(.useCredential, urlCredential)
    }
    
    //MARK: Docoder
    private func decoder<T: GraphDecoder>(data: Data?, responseType: T.Type) -> T? {
        if let _data = data {
            let json = JSONUtils.dataToDictionary(data: _data)
            let decoded = responseType.init(json: json!)
            
            return decoded
        }
        
        return nil
    }
    
    //MARK: Handle Error
    private func handleError(with json: [String: Any]) -> GraphError? {
        let error = GraphError(json: json)
        return error
    }

    private func handleLocalError(message: String?, code: Int) -> GraphError? {
        var error = GraphError()
        error.message = message
        error.code = code
        error.error = "Local Error"

        return error
    }
}

