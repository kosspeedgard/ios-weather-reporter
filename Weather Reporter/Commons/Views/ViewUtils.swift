//
//  ViewUtils.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import UIKit

class ViewUtils: NSObject {
    static func callViewController(withStoryboard storyboard: Storyboard, storyboardId: String) -> UIViewController? {
        let _storyboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        let viewController = _storyboard.instantiateViewController(withIdentifier: storyboardId)
        return viewController
    }
    
    static func goToSpecifyViewController(with navigationController: UINavigationController, to viewController: UIViewController) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            navigationController.pushViewController(viewController, animated: true)
        })
    }
}
