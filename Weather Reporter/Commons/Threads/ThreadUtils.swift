//
//  ThreadUtils.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 21/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

func doInBackground(_ block: @escaping () -> ()) {
    let qos = DispatchQoS.QoSClass.background
    let queue = DispatchQueue.global(qos: qos)
    
    queue.async {
        block()
    }
}

func doOnMain(_ block: @escaping () -> ()) {
    DispatchQueue.main.async {
        block()
    }
}
