//
//  Utils.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

class Utils: NSObject {
    static func timeFromUnix(time: Int?, format: String) -> String? {
        return Date(unix: time ?? 0).string(format: format)
    }
}
