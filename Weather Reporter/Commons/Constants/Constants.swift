//
//  Constants.swift
//  Weather Reporter
//
//  Created by Khwan Siricharoenporn on 22/11/2561 BE.
//  Copyright © 2561 DEVG. All rights reserved.
//

import Foundation

struct Constants {
    struct Alert {
        static let error = "ERROR"
        static let ok = "OK"
    }
    
    struct Wording {
        static let toF = "To °F"
        static let toC = "To °C"
    }
    
    struct TableViewCellIdentifier {
        struct Weather {
            static let forecastTableViewCell = "ForecastTableViewCellIdentifier"
        }
    }
    
    struct TimeFormat {
        static let shortTime = "HH:mm"
    }
}
